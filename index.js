module.exports = createMacro(myMacro);

function myMacro({ references, state, babel }) {
  references.default.forEach(({ parentPath }) => {
    if (parentPath.type === "TaggedTemplateExpression") {
      const quasis = parentPath.node.quasi.quasis.map((element) => element.value.raw);

      const expressions = parentPath.node.quasi.expressions.map(
        (element) => element.value
      );

      const concatedQuasisExpressions = composeStringLiteral(quasis, expressions)

      parentPath.replaceWith(babel.types.stringLiteral(concatedQuasisExpressions));
    }

    if (parentPath.type === "CallExpression") {
      const quasis = parentPath.node.arguments[0].quasis.map(element => element.value.raw)
      const expressions = parentPath.node.arguments[0].expressions.map(element => element.value)
      
      const concatedQuasisExpressions = composeStringLiteral(quasis, expressions)
      
      parentPath.replaceWith(babel.types.stringLiteral(concatedQuasisExpressions));
    }
  });
}

const composeStringLiteral = (strings, expressions) => {
  return strings.reduce(
        (result, currentString, i) =>
          `${result}${currentString}${expressions[i] ? `${expressions[i]}` : ""}`,
        ""
      );
}
